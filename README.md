# Ğ1 F-Droid repo

This repo contain the latest versions of this apps :
* Gchange (Marketplace)
* Cesium (Ğ1 Wallet App)
* Ğecko (ĞDev Wallet App)

To add this repo in F-Droid, add the following url to the *Repositories* section of the app's settings: [url](https://f-droid.duniter.org/fdroid/repo?fingerprint=090EBB8AF7298BB3A5F87A2EB35F16F589F41690498FC6D5FCE959845FD5A91D)

