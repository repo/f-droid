#!/bin/bash
# Checks if any app updates are available and downloads them if there are

set -o noclobber
set -o errexit
set -o nounset

gchange() {
    echo Update gchange...
    UPDATE_URL=https://api.github.com/repos/duniter-gchange/gchange-client/releases/latest
    LATEST_VERSION="$(curl -s $UPDATE_URL | jq -r '.name')"
    echo Latest version is $LATEST_VERSION
    i=0
    for row in $(curl -s $UPDATE_URL | jq -c '.assets[]'); do
        name=$(echo $row | jq '.name')
        if [ ${name: -4} == 'apk"' ]; then
            echo .apk file, use for update
            wget -O fdroid/repo/gchange.apk "$(curl -s $UPDATE_URL | jq -r ".assets[$i].browser_download_url")"
        else
        echo Not .apk file, skip $name
        fi
        i=$i+1
    done
}
cesium() {
    UPDATE_URL=https://api.github.com/repos/duniter/cesium/releases/latest
    LATEST_VERSION="$(curl -s $UPDATE_URL | jq -r '.name')"
    echo Latest version is $LATEST_VERSION
    i=0
    for row in $(curl -s $UPDATE_URL | jq -c '.assets[]'); do
        name=$(echo $row | jq '.name')
        if [ ${name: -4} == 'apk"' ]; then
            wget -O fdroid/repo/cesium.apk "$(curl -s $UPDATE_URL | jq -r ".assets[$i].browser_download_url")"
        else
        echo Not .apk file, skip $name
        fi
        i=$i+1
    done
}
gecko() {
    UPDATE_URL=https://git.duniter.org/api/v4/projects/474/repository/tags
    VERSION_DATA="$(curl -s $UPDATE_URL | jq -r '.[0]')"
    LATEST_VERSION="$(echo $VERSION_DATA | jq -r '.name')"
    APK_URL="https://gecko-apk.p2p.legal/dl/gecko-${LATEST_VERSION:1}-v8a.apk"
    echo Latest version is $LATEST_VERSION
    wget -O fdroid/repo/gecko.apk $APK_URL
}
# Check for updates for applications listed below
cesium
gchange
gecko
